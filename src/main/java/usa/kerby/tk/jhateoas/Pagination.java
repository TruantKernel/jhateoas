package usa.kerby.tk.jhateoas;

import usa.kerby.tk.pageable.Pageable;

/**
 * @author Trevor Kerby
 * @since Feb 9, 2020
 */
public class Pagination implements Pageable, DataTransferObject {
	public int size;
	public int limit;
	public int offset;

	public Pagination(int size, int offset, int limit) {
		this.limit = limit;
		this.offset = offset;
		this.size = size;
	}

	@Override
	public int getLimit() {
		return this.limit;
	}

	@Override
	public int getOffset() {
		return this.offset;
	}

	@Override
	public int getSize() {
		return this.size;
	}

}
