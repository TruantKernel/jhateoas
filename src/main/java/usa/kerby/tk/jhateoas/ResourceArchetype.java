package usa.kerby.tk.jhateoas;

/**
 * @author Trevor Kerby
 * @since Mar 24, 2021
 */
public enum ResourceArchetype {

	DOCUMENT(ResourceArchetype.DOCUMENT_NAME),
	COLLECTION(ResourceArchetype.COLLECTION_NAME),
	STORE(ResourceArchetype.STORE_NAME),
	CONTROLLER(ResourceArchetype.CONTROLLER_NAME);

	public static final String DOCUMENT_NAME = "document";
	public static final String COLLECTION_NAME = "collection";
	public static final String STORE_NAME = "store";
	public static final String CONTROLLER_NAME = "controller";

	private final String name;

	private ResourceArchetype(String name) {
		this.name = name;
	}

	public boolean equalsName(String otherName) {
		return name.equals(otherName);
	}

	public String toString() {
		return this.name;
	}

}
