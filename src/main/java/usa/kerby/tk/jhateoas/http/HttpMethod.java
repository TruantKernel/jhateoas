package usa.kerby.tk.jhateoas.http;

/**
 * @author Trevor Kerby
 * @since Aug 2, 2020
 */
public enum HttpMethod {
	GET("GET"),

	POST("POST"),

	PUT("PUT"),

	DELETE("DELETE"),

	PATCH("PATCH"),

	HEAD("HEAD"),

	OPTIONS("OPTIONS");

	private final String name;

	private HttpMethod(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		// (otherName == null) check is not needed because name.equals(null) returns false
		return name.equals(otherName);
	}

	public String toString() {
		return this.name;
	}

}
