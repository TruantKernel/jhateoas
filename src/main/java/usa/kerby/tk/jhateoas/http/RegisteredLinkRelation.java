package usa.kerby.tk.jhateoas.http;

/**
 * Based entirely off of IANA registered Link Relations and their documentation
 * https://www.iana.org/assignments/link-relations/link-relations.xhtml
 */
public enum RegisteredLinkRelation implements LinkRelation {

	/**
	 * Refers to a resource that is the subject of the link's context.
	 */
	ABOUT("about"),

	/**
	 * Refers to a substitute for this context
	 */
	ALTERNATE("alternate"),

	/**
	 * Used to reference alternative content that uses the AMP profile of the HTML format.
	 */
	AMPHTML("amphtml"),

	/**
	 * Refers to an appendix.
	 */
	APPENDIX("appendix"),

	/**
	 * Refers to an icon for the context. Synonym for icon.
	 */
	APPLE_TOUCH_ICON("apple-touch-icon"),

	/**
	 * Refers to a launch screen for the context.
	 * 
	 */
	APPLE_TOUCH_STARTUP_IMAGE("apple-touch-startup-image"),

	/**
	 * Refers to a collection of records, documents, or other
	 * materials of historical interest.
	 */
	ARCHIVES("archives"),

	/**
	 * Refers to the context's author.
	 */
	AUTHOR("author"),

	/**
	 * Identifies the entity that blocks access to a resource
	 * following receipt of a legal demand.
	 */
	BLOCKED_BY("blocked-by"),

	/**
	 * Gives a permanent link to use for bookmarking purposes.
	 */
	BOOKMARK("bookmark "),

	/**
	 * Designates the preferred version of a resource (the IRI and its contents).
	 */
	CANONICAL("canonical"),

	/**
	 * Refers to a chapter in a collection of resources.
	 */
	CHAPTER("chapter"),

	/**
	 * Indicates that the link target is preferred over the link context for the purpose of permanent citation.
	 */
	CITE_AS("cite-as"),

	/**
	 * The target IRI points to a resource which represents the collection resource for the context IRI.
	 */
	COLLECTION("collection"),

	/**
	 * Refers to a table of contents.
	 */
	CONTENTS("contents"),

	/**
	 * The document linked to was later converted to the
	 * document that contains this link relation. For example, an RFC can
	 * have a link to the Internet-Draft that became the RFC; in that case,
	 * the link relation would be ""convertedFrom"".
	 * 
	 */
	CONVERTED_FROM("convertedFrom"),

	/**
	 * Refers to a copyright statement that applies to the
	 * link's context.
	 * 
	 */
	COPYRIGHT("copyright"),

	/**
	 * The target IRI points to a resource where a submission form can be obtained.
	 */
	CREATE_FORM("create-form"),

	/**
	 * Refers to a resource containing the most recent
	 * item(s) in a collection of resources.
	 */
	CURRENT("current"),

	/**
	 * Refers to a resource providing information about the
	 * link's context.
	 */
	DESCRIBED_BY("describedby"),

	/**
	 * The relationship A 'describes' B asserts that
	 * resource A provides a description of resource B. There are no
	 * constraints on the format or representation of either A or B,
	 * neither are there any further constraints on either resource.
	 */
	DESCRIBES("describes"),

	/**
	 * Refers to a list of patent disclosures made with respect to
	 * material for which 'disclosure' relation is specified.
	 */
	DISCLOSURE("disclosure"),

	/**
	 * Used to indicate an origin that will be used to fetch required
	 * resources for the link context, and that the user agent ought to resolve
	 * as early as possible.
	 */
	DNS_PREFETCH("dns-prefetch"),

	/**
	 * Refers to a resource whose available representations
	 * are byte-for-byte identical with the corresponding representations of
	 * the context IRI.
	 */
	DUPLICATE("duplicate"),

	/**
	 * Refers to a resource that can be used to edit the
	 * link's context.
	 */
	EDIT("edit"),

	/**
	 * The target IRI points to a resource where a submission form for
	 * editing associated resource can be obtained.
	 */
	EDIT_FORM("edit-form"),

	/**
	 * Refers to a resource that can be used to edit media
	 * associated with the link's context.
	 * 
	 */
	EDIT_MEDIA("edit-media"),

	/**
	 * Identifies a related resource that is potentially
	 * large and might require special handling.
	 */
	ENCLOSURE("enclosure"),

	/**
	 * Refers to a resource that is not part of the same site as the current context.
	 */
	EXTERNAL("external"),

	/**
	 * An IRI that refers to the furthest preceding resource
	 * in a series of resources.
	 */
	FIRST("first"),

	/**
	 * Refers to a glossary of terms.
	 */
	GLOSSARY("glossary"),

	/**
	 * Refers to context-sensitive help.
	 */
	HELP("help"),

	/**
	 * Refers to a resource hosted by the server indicated by
	 * the link context.
	 */
	HOSTS("hosts"),

	/**
	 * Refers to a hub that enables registration for
	 * notification of updates to the context.
	 */
	HUB("hub"),

	/**
	 * Refers to an icon representing the link's context.
	 */
	ICON("icon"),

	/**
	 * Refers to an index.
	 */
	INDEX("index"),

	/**
	 * refers to a resource associated with a time interval that ends before the beginning of the time interval associated with the context resource
	 */
	INTERVAL_AFTER("intervalAfter"),

	/**
	 * refers to a resource associated with a time interval that begins after the end of the time interval associated with the context resource
	 */
	INTERVAL_BEFORE("intervalBefore"),

	/**
	 * refers to a resource associated with a time interval that begins after the beginning of the time interval associated with the context resource,
	 * and ends before the end of the time interval associated with the context resource
	 */
	INTERVAL_CONTAINS("intervalContains"),

	/**
	 * refers to a resource associated with a time interval that begins after the end of the time interval associated with the context resource, or
	 * ends before the beginning of the time interval associated with the context resource
	 */
	INTERVAL_DISJOINT("intervalDisjoint"),

	/**
	 * refers to a resource associated with a time interval that begins before the beginning of the time interval associated with the context
	 * resource, and ends after the end of the time interval associated with the context resource
	 */
	INTERVAL_DURING("intervalDuring"),

	/**
	 * refers to a resource associated with a time interval whose beginning coincides with the beginning of the time interval associated with the
	 * context resource, and whose end coincides with the end of the time interval associated with the context resource
	 */
	INTERVAL_EQUALS("intervalEquals"),

	/**
	 * refers to a resource associated with a time interval that begins after the beginning of the time interval associated with the context resource,
	 * and whose end coincides with the end of the time interval associated with the context resource
	 */
	INTERVAL_FINISHEDBY("intervalFinishedBy"),

	/**
	 * refers to a resource associated with a time interval that begins before the beginning of the time interval associated with the context
	 * resource, and whose end coincides with the end of the time interval associated with the context resource
	 */
	INTERVAL_FINISHES("intervalFinishes"),

	/**
	 * refers to a resource associated with a time interval that begins before or is coincident with the beginning of the time interval associated
	 * with the context resource, and ends after or is coincident with the end of the time interval associated with the context resource
	 */
	INTERVAL_IN("intervalIn"),

	/**
	 * refers to a resource associated with a time interval whose beginning coincides with the end of the time interval associated with the context
	 * resource
	 * 
	 */
	INTERVAL_MEETS("intervalMeets"),

	/**
	 * refers to a resource associated with a time interval whose end coincides with the beginning of the time interval associated with the context
	 * resource
	 * 
	 */
	INTERVAL_MET_BY("intervalMetBy"),

	/**
	 * refers to a resource associated with a time interval that begins before the beginning of the time interval associated with the context
	 * resource, and ends after the beginning of the time interval associated with the context resource
	 * 
	 */
	INTERVAL_OVERLAPPED_BY("intervalOverlappedBy"),

	/**
	 * refers to a resource associated with a time interval that begins before the end of the time interval associated with the context resource, and
	 * ends after the end of the time interval associated with the context resource
	 */
	INTERVAL_OVERLAPS("intervalOverlaps"),

	/**
	 * refers to a resource associated with a time interval whose beginning coincides with the beginning of the time interval associated with the
	 * context resource, and ends before the end of the time interval associated with the context resource
	 * 
	 */
	INTERVAL_STARTED_BY("intervalStartedBy"),

	/**
	 * refers to a resource associated with a time interval whose beginning coincides with the beginning of the time interval associated with the
	 * context resource, and ends after the end of the time interval associated with the context resource
	 */
	INTERVAL_STARTS("intervalStarts"),

	/**
	 * The target IRI points to a resource that is a member of the collection represented by the context IRI.
	 */
	ITEM("item"),

	/**
	 * An IRI that refers to the furthest following resource
	 * in a series of resources.
	 */
	LAST("last"),

	/**
	 * Points to a resource containing the latest (e.g.,
	 * current) version of the context.
	 */
	LATEST_VERSION("latest-version"),

	/**
	 * Refers to a license associated with this context.
	 */
	LICENSE("license"),

	/**
	 * Refers to further information about the link's context,
	 * expressed as a LRDD (""Link-based Resource Descriptor Document"")
	 * resource. See [RFC6415] for information about
	 * processing this relation type in host-meta documents. When used
	 * elsewhere, it refers to additional links and other metadata.
	 * Multiple instances indicate additional LRDD resources. LRDD
	 * resources MUST have an ""application/xrd+xml"" representation, and
	 * MAY have others.
	 */
	LRDD("lrdd"),

	/**
	 * Links to a manifest file for the context.
	 */
	MANIFEST("manifest"),

	/**
	 * Refers to a mask that can be applied to the icon for the context.
	 */
	MASK_ICON("mask-icon"),

	/**
	 * Refers to a feed of personalised media recommendations relevant to the link context.
	 */
	MEDIA_FEED("media-feed"),

	/**
	 * The Target IRI points to a Memento, a fixed resource that will not change state anymore.
	 */
	MEMENTO("memento"),

	/**
	 * Links to the context's Micropub endpoint.
	 */
	MICROPUB("micropub"),

	/**
	 * Refers to a module that the user agent is to preemptively fetch and store for use in the current context.
	 */
	MODULE_PRELOAD("modulepreload"),

	/**
	 * Refers to a resource that can be used to monitor changes in an HTTP resource.
	 */
	MONITOR("monitor"),

	/**
	 * Refers to a resource that can be used to monitor changes in a specified group of HTTP resources.
	 */
	MONITOR_GROUP("monitor-group"),

	/**
	 * Indicates that the link's context is a part of a series, and
	 * that the next in the series is the link target.
	 */
	NEXT("next"),

	/**
	 * Refers to the immediately following archive resource.
	 */
	NEXT_ARCHIVE("next-archive"),

	/**
	 * Indicates that the context's original author or publisher does not endorse the link target.
	 */
	NO_FOLLOW("nofollow"),

	/**
	 * Indicates that any newly created top-level browsing context which results from following the link will not be an auxiliary browsing context.
	 */
	NO_OPENER("noopener"),

	/**
	 * Indicates that no referrer information is to be leaked when following the link.
	 */
	NO_REFERRER("noreferrer"),

	/**
	 * Indicates that any newly created top-level browsing context which results from following the link will be an auxiliary browsing context.
	 */
	OPENER("opener"),

	/**
	 * Refers to an OpenID Authentication server on which the context relies for an assertion that the end user controls an Identifier.
	 */
	OPENID2_LOCAL_ID("openid2.local_id"),

	/**
	 * Refers to a resource which accepts OpenID Authentication protocol messages for the context.
	 */
	OPENID2_PROVIDER("openid2.provider"),

	/**
	 * The Target IRI points to an Original Resource.
	 */
	ORIGINAL("original"),

	/**
	 * Refers to a P3P privacy policy for the context.
	 */
	P3PV1("P3Pv1"),

	/**
	 * Indicates a resource where payment is accepted.
	 */
	PAYMENT("payment"),

	/**
	 * Gives the address of the pingback resource for the link context.
	 */
	PINGBACK("pingback"),

	/**
	 * Used to indicate an origin that will be used to fetch required
	 * resources for the link context. Initiating an early connection, which
	 * includes the DNS lookup, TCP handshake, and optional TLS negotiation,
	 * allows the user agent to mask the high latency costs of establishing a
	 * connection.
	 */
	PRECONNECT("preconnect"),

	/**
	 * Points to a resource containing the predecessor
	 * version in the version history.
	 */
	PREDECESSOR_VERSION("predecessor-version"),

	/**
	 * The prefetch link relation type is used to identify a resource
	 * that might be required by the next navigation from the link context, and
	 * that the user agent ought to fetch, such that the user agent can deliver a
	 * faster response once the resource is requested in the future.
	 */
	PREFETCH("prefetch"),

	/**
	 * Refers to a resource that should be loaded early in the
	 * processing of the link's context, without blocking rendering.
	 */
	PRELOAD("preload"),

	/**
	 * Used to identify a resource that might be required by the next
	 * navigation from the link context, and that the user agent ought to fetch
	 * and execute, such that the user agent can deliver a faster response once
	 * the resource is requested in the future.
	 */
	PRERENDER("prerender"),

	/**
	 * Indicates that the link's context is a part of a series, and
	 * that the previous in the series is the link target.
	 */
	PREV("prev"),

	/**
	 * Refers to a resource that provides a preview of the link's context.
	 */
	PREVIEW("preview"),

	/**
	 * Refers to the previous resource in an ordered series
	 * of resources. Synonym for "prev".
	 * 
	 */
	PREVIOUS("previous"),

	/**
	 * Refers to the immediately preceding archive resource.
	 * 
	 */
	PREV_ARCHIVE("prev-archive"),

	/**
	 * Refers to a privacy policy associated with the link's context.
	 */
	PRIVACY_POLICY("privacy-policy"),

	/**
	 * Identifying that a resource representation conforms
	 * to a certain profile, without affecting the non-profile semantics
	 * of the resource representation.
	 */
	PROFILE("profile"),

	/**
	 * Links to a publication manifest. A manifest represents
	 * structured information about a publication, such as informative metadata,
	 * a list of resources, and a default reading order.
	 */
	PUBLICATION("publication"),

	/**
	 * Identifies a related resource.
	 */
	RELATED("related"),

	/**
	 * Identifies the root of RESTCONF API as configured on this HTTP server.
	 * The "restconf" relation defines the root of the API defined in RFC8040.
	 * Subsequent revisions of RESTCONF will use alternate relation values to support
	 * protocol versioning.
	 * 
	 */
	RESTCONF("restconf"),

	/**
	 * Identifies a resource that is a reply to the context
	 * of the link.
	 */
	REPLIES("replies"),

	/**
	 * Refers to a resource that can be used to search through
	 * the link's context and related resources.
	 * 
	 */
	SEARCH("search"),

	/**
	 * Refers to a section in a collection of resources.
	 */
	SECTION("section"),

	/**
	 * Conveys an identifier for the link's context.
	 */
	SELF("self"),

	/**
	 * Indicates a URI that can be used to retrieve a
	 * service document.
	 */
	SERVICE("service"),

	/**
	 * Identifies service description for the context that
	 * is primarily intended for consumption by machines.
	 */
	SERVICE_DESC("service-desc"),

	/**
	 * Identifies service documentation for the context that
	 * is primarily intended for human consumption.
	 */
	SERVICE_DOC("service-doc"),

	/**
	 * Identifies general metadata for the context that is
	 * primarily intended for consumption by machines.
	 */
	SERVICE_META("service-meta"),

	/**
	 * Refers to a resource that is within a context that is
	 * sponsored (such as advertising or another compensation agreement).
	 */
	SPONSORED("sponsored"),

	/**
	 * Refers to the first resource in a collection of
	 * resources.
	 */
	START("start"),

	/**
	 * Identifies a resource that represents the context's
	 * status.
	 */
	STATUS("status"),

	/**
	 * Refers to a stylesheet.
	 */
	STYLESHEET("stylesheet"),

	/**
	 * Refers to a resource serving as a subsection in a
	 * collection of resources.
	 */
	SUBSECTION("subsection"),

	/**
	 * Points to a resource containing the successor version
	 * in the version history.
	 */
	SUCCESSOR_VERSION("successor-version"),

	/**
	 * Identifies a resource that provides information about
	 * the context's retirement policy.
	 */
	SUNSET("sunset"),

	/**
	 * Gives a tag (identified by the given address) that applies to
	 * the current document.
	 */
	TAG("tag"),

	/**
	 * Refers to the terms of service associated with the link's context.
	 */
	TERMS_OF_SERVICE("terms-of-service"),

	/**
	 * The Target IRI points to a TimeGate for an Original Resource.
	 */
	TIMEGATE("timegate"),

	/**
	 * The Target IRI points to a TimeMap for an Original Resource.
	 */
	TIMEMAP("timemap"),

	/**
	 * Refers to a resource identifying the abstract semantic type of which the link's context is considered to be an instance.
	 */
	TYPE("type"),

	/**
	 * Refers to a resource that is within a context that is User Generated Content.
	 */
	UGC("ugc"),

	/**
	 * Refers to a parent document in a hierarchy of
	 * documents.
	 */
	UP("up"),

	/**
	 * Points to a resource containing the version history
	 * for the context.
	 */
	VERSION_HISTORY("version-history"),

	/**
	 * Identifies a resource that is the source of the
	 * information in the link's context.
	 */
	VIA("via"),

	/**
	 * Identifies a target URI that supports the Webmention protocol.
	 * This allows clients that mention a resource in some form of publishing process
	 * to contact that endpoint and inform it that this resource has been mentioned.
	 */
	WEBMENTION("webmention"),

	/**
	 * Points to a working copy for this resource.
	 */
	WORKING_COPY("working-copy"),

	/**
	 * Points to the versioned resource from which this
	 * working copy was obtained.
	 */
	WORKING_COPY_OF("working-copy-of");

	private final String name;

	private RegisteredLinkRelation(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		// (otherName == null) check is not needed because name.equals(null) returns false
		return name.equals(otherName);
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
