package usa.kerby.tk.jhateoas.http;

import jakarta.json.bind.serializer.JsonbSerializer;
import jakarta.json.bind.serializer.SerializationContext;
import jakarta.json.stream.JsonGenerator;

/**
 * @author Trevor Kerby
 * @since Aug 9, 2020
 */
public class HyperlinkSerializer implements JsonbSerializer<Hyperlink> {

	@Override
	public void serialize(Hyperlink link, JsonGenerator generator, SerializationContext ctx) {
		//generator.writeStartObject();
		if (link.getName() != null) {
			generator.write("name", link.getName());
		}
		if (link.isTemplated()) {
			generator.write("templated", true);
		}
		generator.write("href", link.getHref());
		if (link.getTitle() != null) {
			generator.write("title", link.getTitle());
		}
		if (link.getDeprecation() != null) {
			generator.write("deprecation", link.getDeprecation());
		}
		if (link.getHreflang() != null) {
			generator.write("hreflang", link.getHreflang());
		}
		if (link.getProfile() != null) {
			generator.write("profile", link.getProfile());
		}
		if (link.getType() != null) {
			generator.write("type", link.getType());
		}
		//generator.writeEnd();
	}
}
